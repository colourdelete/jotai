package handle

import (
	"errors"
	"fmt"
	"github.com/google/shlex"
	"gitlab.com/colourdelete/jotai/pkg/jotai"
	"log"
	"strconv"
	"strings"
)

type Handler struct {
	logger    *log.Logger
	sender    jotai.Sender
	prefix    string
	statusers map[string]jotai.Statuser
}

func NewHandler(logger *log.Logger, sender jotai.Sender, prefix string, statusers map[string]jotai.Statuser) *Handler {
	if prefix == "" { // first arg should never be "", so there should be no collision
		prefix = "jotai"
	}
	return &Handler{
		logger:    logger,
		sender:    sender,
		prefix:    prefix,
		statusers: statusers,
	}
}

// https://stackoverflow.com/questions/41602230/return-first-n-chars-of-a-string
// not exporting to avoid https://github.com/aristanetworks/goarista/tree/master/monotime
func firstNChars(s string, n int) string {
	r := []rune(s)
	if len(r) > n {
		return string(r[:n])
	}

	return s
}

func (h *Handler) Handle(m jotai.Message) error {
	args, err := shlex.Split(m.Content)
	if err != nil {
		return err
	}
	// <prefix> discord online 😀 message
	// <prefix> discord online 😀
	// <prefix> discord online
	if args[0] != h.prefix {
		return nil // not intended for the bot
	}

	var online bool
	var icon rune
	var msg string
	var platforms []string

	if len(args) <= 1 {
		return errors.New("length of args is less than 2")
	}

	if len(args) >= 2 {
		platforms = strings.Split(strings.ToLower(args[0]), `,`)
		online = args[1] == `online` || args[1] == `on` // all others are considered offline
	}

	if len(args) >= 3 {
		tmp, err := strconv.Unquote(`"` + args[2] + `"`)
		if err != nil {
			return errors.New("icon cannot be unquoted")
		}
		icon = rune(tmp[0])
	}

	if len(args) >= 4 {
		msg = args[2]
	}

	var statuser jotai.Statuser
	var ok, all bool
	var cache = map[string]struct{}{}

	// remove duplicates and check for *
	for _, platform := range platforms {
		if platform == `*` {
			all = true
			break // since everything is selected, no need to loop over anything else
		}
		cache[platform] = struct{}{}
	}

	if all {
		for _, statuser := range h.statusers {
			statuser.SetStatus(online, icon, msg)
		}
	} else {
		for platform := range cache {
			statuser, ok = h.statusers[platform]
			if !ok {
				return fmt.Errorf(`platform %s not found`, platform)
			}
			statuser.SetStatus(online, icon, msg)
		}

	}

	return h.sender.Send(jotai.Message{
		To: m.From,
		Content: fmt.Sprintf(
			`Status set to %s %s %s on %v platform(s).`,
			map[bool]string{true: `(online)`, false: `(offline)`}[online],
			icon,
			firstNChars(msg, 10),
			map[bool]int{true: len(h.statusers), false: len(cache)}[all],
		),
	})
}
