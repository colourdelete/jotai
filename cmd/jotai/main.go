package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"

	"github.com/pelletier/go-toml"

	"gitlab.com/colourdelete/jotai/pkg/cmd"
	"gitlab.com/colourdelete/jotai/pkg/handle"
	"gitlab.com/colourdelete/jotai/pkg/jotai"

	_ "gitlab.com/colourdelete/jotai/pkg/discord"
	//_ "gitlab.com/colourdelete/jotai/pkg/slack"
)

var flagSet = flag.NewFlagSet("run", flag.ExitOnError) //nolint:gochecknoglobals

var configPath = flagSet.String( //nolint:gochecknoglobals
	"cfg",
	"./config.toml",
	"path of config file",
)

type Config struct {
	Clients map[string]jotai.Config `toml:"clients"`
}

func init() { //nolint:gochecknoinits
	log.SetFlags(
		log.Ldate | log.Lmicroseconds | log.LUTC | log.Llongfile | log.Lmsgprefix,
	)
	cmd.RegisterSubcommand(flagSet, handler_)
}

func handler_() {
	cfg := Config{}

	data, err := ioutil.ReadFile(*configPath)
	if err != nil {
		log.Fatalf(`while reading cfg file: %s`, err)
	}

	err = toml.Unmarshal(data, &cfg)
	if err != nil {
		log.Fatalf(`while unmarshalling cfg file: %s`, err)
	}

	var wg sync.WaitGroup

	statusers := map[string]jotai.Statuser{}

	for name, p := range jotai.Platforms() {
		statusers[name] = p.Statuser
	}

	for name, p := range jotai.Platforms() {
		wg.Add(1)

		prefix := fmt.Sprintf("[clients.%s] ", name)
		logger := log.New(os.Stderr, prefix, log.Flags())
		logger.Print(`starting...`)

		// Configuration
		c, ok := cfg.Clients[name]
		if !ok {
			logger.Print(`invalid config: nonexistent`)
			continue
		}

		if c.Heartbeat <= 0 {
			logger.Print(`heartbeat is zero; fallback to 1 minute`)
			c.Heartbeat = 1 * time.Minute
		}

		handler := handle.NewHandler(
			log.New(os.Stderr, "[handler]", log.Flags()),
			p.Sender,
			prefix,
			statusers,
		)

		go p.HandlerFunc(&wg, c, handler)
	}

	log.Print("waiting until goroutines finish...")
	wg.Wait()
	log.Print("all goroutines finished.")
	os.Exit(2)
}

func main() {
	handler, err := cmd.Parse(os.Args)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	handler()
}
