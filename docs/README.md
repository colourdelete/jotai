# Jotai

Jotai is a program to manage all your statuses in one place.

You can set up your own instance of Jotai with custom settings like platforms and replies.

## Bot Instance Hosted by gitlab.com/colourdelete (author)

### Platforms

- [ ] Discord ([OAuth2](https://discord.com/api/oauth2/authorize?client_id=759238265592348753&permissions=2048&redirect_uri=https%3A%2F%2Fcolourdelete.gitlab.io%2Fjotai%2Fredirect%2Fdiscord&response_type=code&scope=identify%20bot))
- [ ] Slack

