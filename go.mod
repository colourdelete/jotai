module gitlab.com/colourdelete/jotai

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.1
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/jessevdk/go-flags v1.4.0 // indirect
	github.com/pelletier/go-toml v1.8.1
	github.com/slack-go/slack v0.7.4
)
